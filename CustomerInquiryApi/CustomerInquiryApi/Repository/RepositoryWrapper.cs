﻿using AppEF.IRepository;
using AppEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEF.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private CustomerContext _repoContext;
        private ICustomerRepository _customer;
        private ITransactionRepository _tran;

        public RepositoryWrapper(CustomerContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

        public ICustomerRepository Customer {

            get
            {
                if (_customer == null)
                {
                    _customer = new CustomerRepository(_repoContext);
                }

                return _customer;
            }
        }

        public ITransactionRepository Transaction
        {
            get
            {
                if (_tran == null)
                {
                    _tran = new TransactionRepository(_repoContext);
                }

                return _tran;
            }
        }
    }
}
