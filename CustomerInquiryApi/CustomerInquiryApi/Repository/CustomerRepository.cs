﻿using AppEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEF.IRepository;
using Microsoft.EntityFrameworkCore;

namespace AppEF.Repository
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(CustomerContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<Customer>> GetAllCustomerAsync()
        {
            var cuastomer = await FindAllAsync();
            return cuastomer.OrderBy(x => x.CustomerName);
        }

        public async Task<CustomerDTO> GetCustomerByEmailAsync(string email)
        {           
            Customer obj = await RepositoryContext.Customers.Where(_ => _.ContactEmail == email).Include(_ => _.Transactions).FirstOrDefaultAsync();

            if (obj != null)
            {
                CustomerDTO cdto = new CustomerDTO
                {
                    CustomerID = obj.Id,
                    CustomerName = obj.CustomerName,
                    ContactEmail = obj.ContactEmail,
                    MobileNo = obj.MobileNo,
                    Transactions = obj.Transactions.ToList().ConvertAll(_ => new TransactionDTO { TransactionID = _.Id, TransactionDateTime = _.TransactionDateTime, Amount = _.Amount, CurrencyCode = _.CurrencyCode, Status = _.Status })

                };

                return cdto;
            }
            return null;
        }

        public async Task<CustomerDTO> GetCustomerByIdAndEmailAsync(int id, string email)
        {           
            Customer obj = await RepositoryContext.Customers.Where(_ => _.Id == id || _.ContactEmail == email).Include(_ => _.Transactions).FirstOrDefaultAsync();

            if (obj != null)
            {
                CustomerDTO cdto = new CustomerDTO
                {
                    CustomerID = obj.Id,
                    CustomerName = obj.CustomerName,
                    ContactEmail = obj.ContactEmail,
                    MobileNo = obj.MobileNo,
                    Transactions = obj.Transactions.ToList().ConvertAll(_ => new TransactionDTO { TransactionID = _.Id, TransactionDateTime = _.TransactionDateTime, Amount = _.Amount, CurrencyCode = _.CurrencyCode, Status = _.Status })

                };

                return cdto;
            }
            return null;
        }

        public async Task<CustomerDTO> GetCustomerByIdAsync(int Id)
        {            
            Customer obj = await RepositoryContext.Customers.Where(_ => _.Id == Id).Include(_ => _.Transactions).FirstOrDefaultAsync();

            if (obj != null)
            {
                CustomerDTO cdto = new CustomerDTO
                {
                    CustomerID = obj.Id,
                    CustomerName = obj.CustomerName,
                    ContactEmail = obj.ContactEmail,
                    MobileNo = obj.MobileNo,
                    Transactions = obj.Transactions.ToList().ConvertAll(_ => new TransactionDTO { TransactionID = _.Id, TransactionDateTime = _.TransactionDateTime, Amount = _.Amount, CurrencyCode = _.CurrencyCode, Status = _.Status })

                };
                return cdto;
            }

            return null;
        }
    }
}
