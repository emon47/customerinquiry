﻿using AppEF.IRepository;
using AppEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace AppEF.Repository
{
    public class TransactionRepository : RepositoryBase<AppEF.Model.Transaction>, ITransactionRepository
    {
        public TransactionRepository(CustomerContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<AppEF.Model.Transaction>> GetAllTransactionAsync()
        {
            var transaction = await FindAllAsync();
            return transaction.OrderBy(x => x.CustomerID);
        }

        public async Task<AppEF.Model.Transaction> GetTransactionByIdAsync(int Id)
        {
            var transaction = await FindByConditionAsync(o => o.Id.Equals(Id));
            return transaction.DefaultIfEmpty(new AppEF.Model.Transaction())
                    .FirstOrDefault();
        }        
    }
}