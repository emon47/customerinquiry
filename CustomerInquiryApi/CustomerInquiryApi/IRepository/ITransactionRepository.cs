﻿using AppEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEF.IRepository
{
    public interface ITransactionRepository : IRepositoryBase<Transaction>
    {
        Task<IEnumerable<Transaction>> GetAllTransactionAsync();
        Task<Transaction> GetTransactionByIdAsync(int id);
    }
}
