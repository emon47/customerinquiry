﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEF.IRepository
{
    public interface IRepositoryWrapper
    {
        ICustomerRepository Customer { get; }
        ITransactionRepository Transaction { get; }
    }
}
