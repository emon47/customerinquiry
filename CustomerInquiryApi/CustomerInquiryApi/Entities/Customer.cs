﻿using AppEF.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AppEF.Model
{
    public class Customer : IEntity
    {
        [Key]
        [Column("CustomerID")]
        [MaxLength(10)]
        public int Id { get; set; }
        [MaxLength(30)]
        public string CustomerName { get; set; }
        [MaxLength(25)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string ContactEmail { get; set; }
        [StringLength(10)]
        public string MobileNo { get; set; }
        
        public virtual ICollection<Transaction> Transactions { get; set; }       
    }

    public class CustomerDTO
    {        
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string ContactEmail { get; set; }
        public string MobileNo { get; set; }

        public  List<TransactionDTO> Transactions { get; set; }
    }
}
