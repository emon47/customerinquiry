﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEF.Model
{
    public class CustomerContext: DbContext
    {
        public CustomerContext(DbContextOptions options)
            : base(options)
        {
        }      

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasData(
       new Customer() { Id = 123456, CustomerName = "Firstname Lastname1", ContactEmail = "user1@domain.com", MobileNo= "0123456788" },
       new Customer() { Id = 123457, CustomerName = "Firstname Lastname2", ContactEmail = "user2@domain.com", MobileNo = "0123456799" },
       new Customer() { Id = 123458, CustomerName = "Firstname Lastname3", ContactEmail = "user3@domain.com", MobileNo = "0123456777" });

            modelBuilder.Entity<Transaction>().HasData(
       new Transaction() { Id = 1, TransactionDateTime = DateTime.Now, Amount = 100, CurrencyCode = "USD", Status= "Success", CustomerID = 123457 },
       new Transaction() { Id = 2, TransactionDateTime = DateTime.Now, Amount = 200, CurrencyCode = "USD", Status = "Success", CustomerID = 123458 },
       new Transaction() { Id = 3, TransactionDateTime = DateTime.Now, Amount = 300, CurrencyCode = "THB", Status = "Failed", CustomerID = 123458 });

        }
    }
}
