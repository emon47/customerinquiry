USE [CustomerInquiryDb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 4/2/2019 1:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 4/2/2019 1:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](30) NULL,
	[ContactEmail] [nvarchar](25) NULL,
	[MobileNo] [nvarchar](10) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 4/2/2019 1:30:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionDateTime] [datetime2](7) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[CurrencyCode] [nvarchar](max) NULL,
	[Status] [nvarchar](max) NULL,
	[CustomerID] [int] NOT NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190401170500_Migration1', N'2.1.8-servicing-32085')
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CustomerID], [CustomerName], [ContactEmail], [MobileNo]) VALUES (123456, N'Firstname Lastname1', N'user1@domain.com', N'0123456788')
INSERT [dbo].[Customers] ([CustomerID], [CustomerName], [ContactEmail], [MobileNo]) VALUES (123457, N'Firstname Lastname2', N'user2@domain.com', N'0123456799')
INSERT [dbo].[Customers] ([CustomerID], [CustomerName], [ContactEmail], [MobileNo]) VALUES (123458, N'Firstname Lastname3', N'user3@domain.com', N'0123456777')
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[Transactions] ON 

INSERT [dbo].[Transactions] ([TransactionID], [TransactionDateTime], [Amount], [CurrencyCode], [Status], [CustomerID]) VALUES (1, CAST(N'2019-04-02T01:04:59.8080000' AS DateTime2), CAST(100.00 AS Decimal(18, 2)), N'USD', N'Success', 123457)
INSERT [dbo].[Transactions] ([TransactionID], [TransactionDateTime], [Amount], [CurrencyCode], [Status], [CustomerID]) VALUES (2, CAST(N'2019-04-02T01:04:59.8100000' AS DateTime2), CAST(200.00 AS Decimal(18, 2)), N'USD', N'Success', 123458)
INSERT [dbo].[Transactions] ([TransactionID], [TransactionDateTime], [Amount], [CurrencyCode], [Status], [CustomerID]) VALUES (3, CAST(N'2019-04-02T01:04:59.8100000' AS DateTime2), CAST(300.00 AS Decimal(18, 2)), N'THB', N'Failed', 123458)
SET IDENTITY_INSERT [dbo].[Transactions] OFF
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [FK_Transactions_Customers_CustomerID] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [FK_Transactions_Customers_CustomerID]
GO
