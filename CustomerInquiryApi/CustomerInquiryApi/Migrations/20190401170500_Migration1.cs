﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CustomerInquiryApi.Migrations
{
    public partial class Migration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustomerID = table.Column<int>(maxLength: 10, nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerName = table.Column<string>(maxLength: 30, nullable: true),
                    ContactEmail = table.Column<string>(maxLength: 25, nullable: true),
                    MobileNo = table.Column<string>(maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerID);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    TransactionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TransactionDateTime = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    CustomerID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.TransactionID);
                    table.ForeignKey(
                        name: "FK_Transactions_Customers_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "Customers",
                        principalColumn: "CustomerID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerID", "ContactEmail", "CustomerName", "MobileNo" },
                values: new object[] { 123456, "user1@domain.com", "Firstname Lastname1", "0123456788" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerID", "ContactEmail", "CustomerName", "MobileNo" },
                values: new object[] { 123457, "user2@domain.com", "Firstname Lastname2", "0123456799" });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "CustomerID", "ContactEmail", "CustomerName", "MobileNo" },
                values: new object[] { 123458, "user3@domain.com", "Firstname Lastname3", "0123456777" });

            migrationBuilder.InsertData(
                table: "Transactions",
                columns: new[] { "TransactionID", "Amount", "CurrencyCode", "CustomerID", "Status", "TransactionDateTime" },
                values: new object[] { 1, 100m, "USD", 123457, "Success", new DateTime(2019, 4, 2, 1, 4, 59, 808, DateTimeKind.Local) });

            migrationBuilder.InsertData(
                table: "Transactions",
                columns: new[] { "TransactionID", "Amount", "CurrencyCode", "CustomerID", "Status", "TransactionDateTime" },
                values: new object[] { 2, 200m, "USD", 123458, "Success", new DateTime(2019, 4, 2, 1, 4, 59, 810, DateTimeKind.Local) });

            migrationBuilder.InsertData(
                table: "Transactions",
                columns: new[] { "TransactionID", "Amount", "CurrencyCode", "CustomerID", "Status", "TransactionDateTime" },
                values: new object[] { 3, 300m, "THB", 123458, "Failed", new DateTime(2019, 4, 2, 1, 4, 59, 810, DateTimeKind.Local) });

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CustomerID",
                table: "Transactions",
                column: "CustomerID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}
