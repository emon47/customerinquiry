﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEF.IRepository;
using AppEF.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppEF.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CustomerInquiryController : ControllerBase
    {
        private IRepositoryWrapper _repository;

        public CustomerInquiryController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }              

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
               return Ok("No inquiry criteria");
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
               
        [Route("GetCustomerById")]
        [HttpGet]
        public async Task<IActionResult> GetCustomerById(int id)
        {           
            try
            {
                var customer = await _repository.Customer.GetCustomerByIdAsync(id);
                if (customer == null)
                {
                  return  Ok("Not found");
                }
                else
                {
                    return Ok(customer);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("GetCustomerByEmail")]
        [HttpGet]
        public async Task<IActionResult> GetCustomerByEmail(string email)
        {           
            try
            {
                var customer = await _repository.Customer.GetCustomerByEmailAsync(email);
                if (customer == null)
                {
                    return Ok("Not found");
                }
                else
                {
                    return Ok(customer);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [Route("GetCustomerByIdAndEmail")]
        [HttpGet]
        public async Task<IActionResult> GetCustomerByIdAndEmail(int id, string email)
        {           
            try
            {
                var customer = await _repository.Customer.GetCustomerByIdAndEmailAsync(id, email);
                if (customer == null)
                {
                    return Ok("Not found");
                }
                else
                {
                    return Ok(customer);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
